Version: 1.0

**DESCRIPCIÓN:** script para instalar, configurar y desintalar automaticamente el software Firma Sofe

**COPYRIGHT:** (C) 2019 Andres MM Alexis <@Alexis98>
                                                                 

Para el correcto funcionamiento de los scripts realizados se debe:              


1. Acceder a la Terminal del Equipo como superusuario
2. Copiar unica y estrictamente la carpeta sofe contenida en el directorio Firma sofe en la ruta /home/(Usuario del equipo)/Documentos
3. Se debe Escribir en la Terminal el comando "_bash install_sofe.sh_" y para desintalar el comando sera "_bash desinstall_sofe.sh_"         
										  